// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

fun create_validator(validator) {
	parallel = PARALLEL(validator, Copy({('witness', Witness) : 'ver_witness', ('verdict', Verdict) : 'ver_verdict'}));
	return parallel;
}

fun filter_composition(composition) {
    sequence = SEQUENCE(composition, Copy({('ver_witness', Witness) : 'witness', ('ver_verdict', Verdict) : 'verdict'}));
    return sequence;
}

fun create_sequence_chain(verifier, port_proof, port_alarm) {
	copy = Copy({('verdict', Verdict) : 'verdict', ('witness', Witness) : 'witness'});
	
	// Filter of ver_* outputs
	port_proof = SEQUENCE(port_proof, copy);	
	port_alarm = SEQUENCE(port_alarm, copy);	

	ite = ITE(ELEMENTOF(verdict, {TRUE}) AND INSTANCEOF(witness, Witness), port_proof, copy);

	iteTwo = ITE(ELEMENTOF(verdict, {FALSE}) AND INSTANCEOF(witness, Witness), port_alarm, copy);

	sequence = SEQUENCE(verifier, ite, iteTwo);
	return sequence;
}

val_success_condition = 'ver_verdict' == 'verdict';

total_success_condition = ELEMENTOF(verdict, {TRUE, FALSE});

verifier_config = ArtifactFactory.create(AtomicActorDefinition, verifier_name);
verifier = ActorFactory.create(ProgramVerifier, verifier_config, "default");

val_cpa_proof = ActorFactory.create(ProgramValidator, "actors/cpachecker.yml", validator_proof_version);
val_uautomizer = ActorFactory.create(ProgramValidator, "actors/uautomizer.yml" , validator_proof_version);

val_cpa_alarm = ActorFactory.create(ProgramValidator, "actors/cpachecker.yml", validator_alarm_version);
val_nitwit = ActorFactory.create(ProgramValidator, "actors/nitwit.yml", validator_alarm_version);
val_symbiotic = ActorFactory.create(ProgramValidator, "actors/symbiotic.yml", validator_alarm_version);
val_fshell = ActorFactory.create(ProgramValidator, "actors/fshell-validator.yml", validator_alarm_version);

val_cpa_proof = create_validator(val_cpa_proof);
val_uautomizer = create_validator(val_uautomizer);

val_cpa_alarm = create_validator(val_cpa_alarm);
val_nitwit = create_validator(val_nitwit);
val_symbiotic = create_validator(val_symbiotic);
val_fshell = create_validator(val_fshell);

val_port_proof = PARALLEL_PORTFOLIO(val_cpa_proof, val_uautomizer, val_success_condition);
val_port_alarm = PARALLEL_PORTFOLIO(val_cpa_alarm, val_fshell, val_nitwit, val_symbiotic, val_success_condition);

val_port_proof = filter_composition(val_port_proof);
val_port_alarm = filter_composition(val_port_alarm);

sequence = create_sequence_chain(verifier, val_port_proof, val_port_alarm);


// Prepare inputs
prog = ArtifactFactory.create(Program, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
inputs = {'program':prog, 'spec':spec};

result = execute(sequence, inputs);
print(result);
