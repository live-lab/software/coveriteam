# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# To execute a job locally, install gitlab-runner (https://docs.gitlab.com/runner/install/)
# and run the following command:
# gitlab-runner exec docker --docker-privileged --docker-volumes /sys/fs/cgroup:/sys/fs/cgroup:rw --env

variables:
  PRIMARY_USER: sosyuser
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/.cache"
  PIP_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/pip"
  CI_MAIN_REGISTRY_IMAGE: "registry.gitlab.com/sosy-lab/software/coveriteam"

.pip-cache: &pip-cache
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - ".cache/pip"

.cvt-cache: &cvt-cache
  cache:
    key: "coveriteam-cache"
    paths:
      - ".cache/"

image: ${CI_MAIN_REGISTRY_IMAGE}/test:python-${PYTHON_VERSION}

stages:
  - images
  - test

.unit-tests: &unit-tests
  stage: test
  before_script:
    # Create user, we do not want to test as root
    - adduser --disabled-login --gecos "" $PRIMARY_USER
    # Give $PRIMARY_USER permission to create cgroups
    - test/for_each_of_my_cgroups.sh chgrp $PRIMARY_USER
    - test/for_each_of_my_cgroups.sh chmod g+w
  script:
    - pytest --version
    - pytest --co -vs -k "${FILTER}" test/
    - sudo -u $PRIMARY_USER XDG_CACHE_HOME=${XDG_CACHE_HOME} pytest --cov coveriteam -vs -k "${FILTER}" test/
  tags:
    - privileged
  only:
    variables:
      - $CI_COMMIT_REF_PROTECTED
  <<: *cvt-cache

url-validity:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: "3.10"
    FILTER: "test_validity_of_download_url"

unit-tests:python-3.8:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: "3.8"
    FILTER: "not test_portfolio and not test_cCegar and not test_validity_of_download_url"

unit-tests:exhaustive:python-3.8:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: "3.8"
    # overwrite default to not FILTER any tests
    FILTER: ""
  only:
    variables:
      - $CI_COMMIT_REF_PROTECTED
    refs:
      - schedules
      - web

unit-tests:python-3.10:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: "3.10"
    FILTER: "not test_portfolio and not test_cCegar and not test_validity_of_download_url"

unit-tests:exhaustive:python-3.10:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: "3.10"
    # overwrite default to not FILTER any tests
    FILTER: ""
  only:
    variables:
      - $CI_COMMIT_REF_PROTECTED
    refs:
      - schedules
      - web

# Static checks
check-format:
  stage: test
  image: python:3.10-bullseye
  before_script:
    - pip install black
  script:
    - black . --check --diff
  <<: *pip-cache

flake8:
  stage: test
  image: python:3.10-bullseye
  before_script:
    - "pip install 'flake8<4' flake8-awesome"
  script:
    - flake8
  <<: *pip-cache

# check license declarations etc.
reuse:
  stage: test
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

# Build Docker images
# following this guideline: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
.build-docker:
  stage: images
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /root/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --dockerfile $CI_PROJECT_DIR/$DOCKERFILE --destination $CI_REGISTRY_IMAGE/$IMAGE
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      changes:
        - $DOCKERFILE
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $BUILD_DOCKER == "true"'

build-docker:test:python-3.8:
  extends: .build-docker
  variables:
    DOCKERFILE: test/Dockerfile.python-3.8
    IMAGE: test:python-3.8

build-docker:test:python-3.10:
  extends: .build-docker
  variables:
    DOCKERFILE: test/Dockerfile.python-3.10
    IMAGE: test:python-3.10
